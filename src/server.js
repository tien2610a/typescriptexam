import express from "express";
import bodyParser from "body-parser";
import viewEngine from "./config/viewEngine";
import initWebRoute from "./route/web";
require('dotenv').config();
//Cau hinh 
let app = express();

// config app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))

viewEngine(app);
initWebRoute(app);

let port = process.env.PORT;

app.listen(port, ()=>{
    //callback
    console.log("api nodejs is running on the port :" +port) 
})

