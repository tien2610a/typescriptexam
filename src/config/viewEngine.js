import express from "express";

let configviewEngine = (app) => {
    //arrow funcion 
    app.use(express.static("../public"))
    app.set("view engine", "ejs");
    app.set("views", "../views")
}

module.exports = configviewEngine;